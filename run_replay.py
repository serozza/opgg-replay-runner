import sys
import re
import subprocess
import os
from random import randint

# settings
binary_name = "League of Legends.exe"
league_of_legends_path = "D:\\Games\\League of Legends\\RADS\\solutions\\lol_game_client_sln\\releases\\0.0.1.247\\deploy"


def find_replay_command(strings_array):
    for line in strings_array:
        result = re.findall("@start.*", line)
        if len(result) != 0:
            return result[0]

    return ""


def read_replay_script():
    if len(sys.argv) > 1:
        file_name = sys.argv[1]
        with open(file_name) as f:
            content = f.readlines()

    return content


def filter_replay_command(start_command):
    start_command = start_command.replace("@start", "")
    start_command = start_command.replace("%locale%", "")
    start_command = start_command.replace("%RANDOM%%RANDOM%", str(randint(9000, 20000)) + str(randint(9000, 20000)))
    start_command = start_command.split("\"")
    del start_command[1]
    del start_command[len(start_command) - 1]
    return [item for item in start_command if item != ' ' and item != '  ']


def main():
    script_text = read_replay_script()

    if len(script_text) == 0:
        print("empty params!")
        exit(1)

    start_command = find_replay_command(script_text)
    if len(start_command) == 0:
        print("start command is not found")
        exit(1)

    filtered_command = filter_replay_command(start_command)

    os.chdir(league_of_legends_path)
    p = subprocess.Popen(filtered_command)
    p.communicate()


if __name__ == '__main__':
    main()
